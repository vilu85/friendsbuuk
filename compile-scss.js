// Import necessary modules
const sass = require('sass');
const fs = require('fs');
const postcss = require('postcss');
const autoprefixer = require('autoprefixer');

// Compile SCSS to CSS
const scssFile = './src/scss/index.scss';
const cssFile = './assets/index.css';

const result = sass.compile(scssFile, {
    style: 'compressed'
});

// Create asset dir if it doesn't exist
if (!fs.existsSync('./assets')) {
    fs.mkdirSync('./assets');
}

// Postprocess the compiled CSS with Autoprefixer
postcss([autoprefixer])
    .process(result.css, { from: scssFile, to: cssFile })
    .then((postcssResult) => {
        fs.writeFileSync(cssFile, postcssResult.css);
    });
