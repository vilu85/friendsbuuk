## Bonus Assignment 11.10: Infinite Friends (Difficult)

This assignment requires some independent googling. The assignment has two parts

1. Build a Friends app. The app should have following features
    1. An API that allows you to add friends and list all existing friends
        - each friend should have a username, a name, and an email address
        - the list of friends can be stored in an in-memory storage
    2. Static front page that uses the API to get a list of friends and displays them on a `<table>`
2. Deploy the Friends app to Azure using either direct code deployment or a Docker container.
3. Build a timer-triggered Azure Function that every ten seconds adds a new friend to the Friends API.
    - Use [NameFake.com API](https://en.namefake.com/api) to generate friends
4. Deploy the function to Azure.
5. Watch in amazement when the friends list keeps growing!
6. Remove the Azure deployments when you're done.

**Hint**: You need to manage connections in Azure functions. The [official documentation](https://learn.microsoft.com/en-us/azure/azure-functions/manage-connections?tabs=javascript) recommends not using external libraries, but if you are not already well-versed in low-level javascript functions and handling streams, just use Axios.
