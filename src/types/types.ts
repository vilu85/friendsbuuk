export interface Friend {
    username: string;
    name: string;
    email: string;
}
