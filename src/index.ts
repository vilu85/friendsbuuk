/**
 * ## Bonus Assignment 11.10: Infinite Friends (Difficult)
 *
 * This assignment requires some independent googling. The assignment has two parts
 *
 * 1. Build a Friends app. The app should have following features
 *     1. An API that allows you to add friends and list all existing friends
 *         - each friend should have a username, a name, and an email address
 *         - the list of friends can be stored in an in-memory storage
 *     2. Static front page that uses the API to get a list of friends and displays them on a `<table>`
 * 2. Deploy the Friends app to Azure using either direct code deployment or a Docker container.
 * 3. Build a timer-triggered Azure Function that every ten seconds adds a new friend to the Friends API.
 *     - Use [NameFake.com API](https://en.namefake.com/api) to generate friends
 * 4. Deploy the function to Azure.
 * 5. Watch in amazement when the friends list keeps growing!
 * 6. Remove the Azure deployments when you're done.
 *
 * **Hint**: You need to manage connections in Azure functions. The [official
 * documentation](https://learn.microsoft.com/en-us/azure/azure-functions/manage-connections?tabs=javascript) recommends
 * not using external libraries, but if you are not already well-versed in low-level javascript functions and handling
 * streams, just use
 * Axios.
 **/
import express from 'express';
import friendsRouter from './api/friends';
import { initMiddlewares, unknownEndpoint } from './api/middlewares';
import axios from 'axios';
import { type Friend } from './types/types';
import path from 'path';

const PORT = process.env.PORT ?? 3000;
const app = express();
const apiPath = '/friends';

// Sets middlewares for the server
initMiddlewares(app);

// Setup routers
app.use(apiPath, friendsRouter);

// Setup the view engine
app.set('view engine', 'ejs');

app.use('/assets', express.static(path.join(__dirname, '../assets')));

// Define a route for the frontend
app.get('/', (_req, res) => {
    axios
        .get<Friend[]>('http://localhost:3000/friends')
        .then((response) => {
            // Extract the friends from the response
            const friends = response.data;

            // Render the template with the friends
            res.render('index', { friends });
        })
        .catch((error) => {
            console.error('Error fetching friends:', error);
            res.status(500).send('Error fetching friends');
        });
});

// Unknown endpoint handler
app.use(unknownEndpoint);

// Starts listening for the port
app.listen(PORT, () => {
    console.log(`Listening to port ${PORT}`);
});
