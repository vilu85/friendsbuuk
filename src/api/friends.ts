import express, { type Request, type Response } from 'express';
import { type Friend } from '../types/types';

const friends: Friend[] = [];

const friendsRouter = express.Router();

// Endpoint for returning all friends
friendsRouter.get('/', (_request: Request, response: Response): void => {
    response.status(200).json(friends);
});

// Endpoint for creating a new friend
friendsRouter.post('/', (request: Request, response: Response): void => {
    const { username, name, email } = request?.body as Friend;

    if (username && name && email) {
        const newFriend: Friend = { username, name, email };
        friends.push(newFriend);
        response.status(201).json(newFriend);
    } else {
        response.status(400).json({ error: 'Missing properties' });
    }
});

// // Endpoint for deleting the event
// friendsRouter.delete('/:eventId', (request: Request, response: Response): void => {
//     const id = request?.params?.eventId;
//     const oldLength = friends.length;
//     friends = friends.filter((event) => event.id !== Number(id));

//     if (friends.length < oldLength) {
//         response.status(204).send();
//     } else {
//         response.status(404).json({ error: `Event id "${id}" does not exist.` });
//     }
// });

// // Endpoint for modifying the event
// friendsRouter.put('/:eventId', (request: Request, response: Response): void => {
//     const id = request?.params?.eventId;
//     const updatedEvent: Event = request.body as Event;

//     let wasModified = false;

//     friends = friends.map((event) => {
//         if (event.id === Number(id)) {
//             wasModified = true;
//             return { ...event, ...updatedEvent };
//         }
//         return event;
//     });

//     if (wasModified) {
//         response.status(200).send();
//     } else {
//         response.status(404).json({ error: `Event id "${id}" does not exist.` });
//     }
// });

// Endpoint for returning events by month
// friendsRouter.get('/:monthNumber', (request: Request, response: Response): void => {
//     const monthNumber = Number(request.params.monthNumber);

//     const result = friends.filter((event) => {
//         const date = new Date(event.date);
//         const month = date.getMonth();
//         return month + 1 === monthNumber;
//     });

//     response.status(200).json(result);
// });

export default friendsRouter;
